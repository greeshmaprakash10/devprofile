package com.unitybees.devprofile

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.math.log

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val bitmap=BitmapFactory.decodeResource(resources,R.drawable.devslopesprofilelogo)
        val rounded=RoundedBitmapDrawableFactory.create(resources,bitmap)

        //TODO if we only need corner rounded imageview, Simple use the below code
        rounded.cornerRadius=15f

        //TODO if you need a complete circular imageview, Then use below code
       // rounded.isCircular=true;
        logo.setImageDrawable(rounded)
    }
}
